# Muneh

## Tests

- [x] $5 + 10 CHF = %10, if exchange rate's 2:1
- [x] $5 + $5 = $10
- [x] $5 * 2 = $10
- [x] Make amount variable private member of the `Dollar` class
- [x] Side effects inside `Dollar` class?
- [x] `equals()`
- [x] Duplicating `Dollar`/`Franc` 
- [x] Common operation `equals()`
- [x] Common operation `times()`
- [x] `Dollar` and `Franc` comparison
- [x] Currency?
- [x] Do we need `testFrancMultiplication()`?
- [x] Bank.reduce(Money)
- [x] Cast `Money` with currency conversion
- [x] Reduce(Bank, String)
- [x] `Sum.plus`
- [x] `Expression.times`